package homework7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class Show7Servlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {		
		try {
			String[] a = new String[5];
			a[0] = "http://step-test-krispop.appspot.com/convert?message=";
			a[1] = "http://step-homework-hnoda.appspot.com/convert?message=";
			a[2] = "http://yuki-stephw7.appspot.com/convert?message=";
			a[3] = "http://1-dot-teeeest0701.appspot.com/convert?message=";
			a[4] = "http://1-dot-step-homework-kitade.appspot.com/convert?message=";

			//resp.setCharacterEncoding("utf-8");
		    for(int i=0;i<5;i++) {
		    	String str = req.getParameter("message");
		    	URL url = new URL(a[i] + str);	
		    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
		    String line;

		    while ((line = reader.readLine()) != null) {
		    	resp.setContentType("text/plain");
				resp.getWriter().println(a[i] + str + "\n" + line);
		    }		    
		    reader.close();
		    }
		} catch (MalformedURLException e) {
			System.out.println("wrong URL");
		} catch (IOException e) {
		    System.out.println("error");
		}
	}
}
